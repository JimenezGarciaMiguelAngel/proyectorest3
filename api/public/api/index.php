<?php

require '../../app/vendor/autoload.php';

$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates'
));
$app->container->singleton('db', function () {
  return new PDO("pgsql:host=localhost user=postgres password=12345 dbname=API port=5432");
});

require '../../app/routes/get.php';
require '../../app/routes/delete.php';
require '../../app/routes/post.php';
require '../../app/routes/put.php';

$app->run();
?>
